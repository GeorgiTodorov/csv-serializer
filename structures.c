// Needed structures for Serialization and Deserialization
typedef struct {
    short rowId;
    char orderId[255];
    char orderDate[255];
    char customerId[255];
    char city[255];
    char state[255];
    int postalCode;
    char region[255];
    char productId[255];
    char category[255];
    char subCategory[255];
    float price;
} Row;
