#include <time.h>
#include <stdio.h>

static int isSilent = 0;
//------------------------------------
// DESCRIPTION: Takes value from argument (s) - silent (0/1)
//
// PARAMETERS: silent(min.c)
//
// RETURN VALUE: NaN
//------------------------------------
void turnSilent(int silent){
    isSilent = silent;
}
//------------------------------------
// DESCRIPTION: Prints out all the logs
// in the app after inputting arguments
// PARAMETERS: fileName - name of the file the log is for
// message - action performed
// RETURN VALUE: NaN/All logs in the app
//------------------------------------
void logs(char *fileName, char *message)
{
    if (isSilent == 0){
        time_t rawtime;
        struct tm *timeinfo;
        char buffer[80];

        time(&rawtime);
        timeinfo = localtime(&rawtime);

        strftime(buffer, 80, "%y/%m/%d %H:%M:%S", timeinfo);
        printf("\n%s [%s] [%s]\n", buffer, fileName, message);
    }
}

