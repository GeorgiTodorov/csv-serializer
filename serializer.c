#include "structures.c"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>


//------------------------------------
// DESCRIPTION: Prints out all the logs
// in the app after inputting arguments
// PARAMETERS: fileName - name of the file the log is for
// message - action performed
// RETURN VALUE: NaN/All logs in the app
//------------------------------------
void logs(char *fileName, char *message);


//------------------------------------
// DESCRIPTION: Function takes one file of type CSV
// opens it to read, and then creates binary file to write in
// Takes out header,rowId to optimize size.
// Reads the csv file through existing structure from (structures.c) and fscanf.
// Writes the binary file through fwrite.
// Counts next content by writing an empty byte between each write.
// Writes content without using extra bytes by using buffer and actual used space.
// PARAMETERS: csvFilePath - Name of the CSV file
// that will be serialized
// RETURN VALUE: 0/1
//------------------------------------
int serialize(char *csvFilePath) {
    // Open CSV File in read mode.
    FILE *csvFile = fopen(csvFilePath, "r");
    //Check if file exists.
    if (!csvFile) {
        printf("Error while opening CSV File\n");
        return 1;
    }

    // Create binary file in write, binary mode.
    FILE *binFile = fopen("ser.bin", "wb");
     //Check if file exists.
    if (!binFile) {
        printf("Error while creating BIN file\n");
        fclose(csvFile);
        return 1;
    }

    //Create an empty char arr to store the header of the CSV file.
    char placeHeader[200];
    logs("serializer.c", "Placeholder for Header Row created.");
    fgets(placeHeader, 200, csvFile);
    size_t headerLength = strlen(placeHeader);
    if (headerLength > 0 && placeHeader[headerLength - 1] == '\n')
    {
        placeHeader[headerLength - 1] = '\0';
    }

    const char *realHeader = "\"Row ID\",\"Order ID\",\"Order Date\",\"Customer ID\",\"City\",\"State\",\"Postal Code\",\"Region\",\"Product ID\",\"Category\",\"Sub-Category\",\"Price\"";
    //Check if the header in the placeholder is equal to the header it expects.
    if (strcmp(placeHeader, realHeader) != 0)
    {
        printf("Mismatch between headers. Error\n");
        return 1;
    }
    logs("serializer.c", "Serialization starting.");
    //Using the existing structure from (structures.c) to read the CSV file storing values properly.
    Row row;
    //Making sure it's == 0.
    memset(&row, '\0', sizeof(row));
    while (fscanf(csvFile, "%hu,%[^,],%[^,],%[^,],%[^,],%[^,],%d,%[^,],%[^,],%[^,],%[^,],%f",
                  &row.rowId, row.orderId, row.orderDate, row.customerId, row.city, row.state,
                  &row.postalCode, row.region, row.productId, row.category, row.subCategory, &row.price) == 12) {
        //byte (=00)
        int zero = 0;
        fwrite(row.orderId+1, strlen(row.orderId)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.orderDate, strlen(row.orderDate), 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.customerId+1, strlen(row.customerId)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.city+1, strlen(row.city)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.state+1, strlen(row.state)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(&row.postalCode, sizeof(row.postalCode), 1, binFile);
        fwrite(row.region+1, strlen(row.region)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.productId+1, strlen(row.productId)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.category+1, strlen(row.category)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(row.subCategory+1, strlen(row.subCategory)-2, 1, binFile);
        fwrite(&zero, 1, 1, binFile);
        fwrite(&row.price, sizeof(row.price), 1, binFile);
        memset(&row, '\0', sizeof(row));
    }
    logs("serializer.c", "Serialization finished.");
    fclose(csvFile);
    fclose(binFile);
    return 0;
}


