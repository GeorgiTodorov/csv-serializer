#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static int silent = 0;

//Valid commands: serialize, deserialize
//Command passed in command line after fileName
static char *command;


//------------------------------------
// DESCRIPTION: Prints out all the logs
// in the app after inputting arguments
// PARAMETERS: fileName - name of the file the log is for
// message - action performed
// RETURN VALUE: NaN/All logs in the app
//------------------------------------
void logs(char *fileName, char *message);

//------------------------------------
// DESCRIPTION: Function serialize from (serializer.c)
//
// PARAMETERS: csvFilePath - Name of the CSV file
// that will be serialized
// RETURN VALUE: 0/1
//------------------------------------
int serialize(char *csvFilePath);

//------------------------------------
// DESCRIPTION: Function setSilen from (log-creator.c)
//
// PARAMETERS: silent - Argument (silent) (0/1)
//
// RETURN VALUE: NaN
//------------------------------------
void turnSilent(int silent);

//------------------------------------
// DESCRIPTION: Function deserialize from (deserializer.c)
//
// PARAMETERS: binFilePath - Name of the Binary file
// that will be deserialized
// RETURN VALUE: 0/1
//------------------------------------
int deserialize(char *binFilePath);


//------------------------------------
// DESCRIPTION: Takes argument values from command line when ran and
// built
// PARAMETERS: file - Name of the CSV/Binary file passed as argument
// argc - Number of strings pointed to by argv (argument count)
// argv - Array of arguments (argument vector)
// RETURN VALUE: NaN
//------------------------------------
void input(char **file, int argc, char *argv[])
{
    //Variable for (True/False) when passing file
    int validFile = 0;
    //Variable for (True/False) when passing argument - command
    int validCommand = 0;
    for (int arg = 1; arg < argc; arg++)
    {
      switch ((argv[arg])[0])
      {
      // Argument 's' - Silent. Sets the app to silent mode and stops logs.
      case 's':
         silent = 1;
         turnSilent(silent);
         break;
      // Argument 'f' - File. Pass the name of the file that will be worked on.
      case 'f':
         arg++;
         *file = argv[arg];
         char* ext = strrchr(*file, '.');
         if (ext == NULL || (strcmp(ext, ".csv") != 0 && strcmp(ext, ".bin") != 0))
         {
            printf("Invalid file extension. Only CSV and binary files are allowed.\n");

         }
         else{
         //True, file is valid
         validFile = 1;
         break;
         }
      // Argument 'c' - Command. Choose which command will be used - serialize/deserialize
      case 'c':
         arg++;
         command = (argv[arg]);
         int trueSerialize = strcmp(command, "serialize");
         int trueDeserialize = strcmp(command, "deserialize");
         if (trueSerialize == 0)
         {
            if (strcmp(ext, ".csv") == 0){
            logs("main.c","Command chosen: Serialization");
            serialize(*file);
            }
            else{
                printf("Did not pass CSV file when command is Serialization\n");

            }
         }
         else if (trueDeserialize == 0)
         {
            if (strcmp(ext, ".bin") == 0)
            {
            logs("main.c","Command chosen: Deserialization");
            deserialize(*file);
            }
            else{
                printf("Did not pass Bin file when command is Deserialization\n");

            }
         }
         else
         {
            printf("Command chosen: INVALID\n");
         }
         validCommand = 1;
         break;
      }
   }
   if (!validFile || !validCommand)
   {
      printf("File path and command are obligatory.\n");
   }
}

//------------------------------------
// DESCRIPTION: Main class, calls input function to take arguments
// which will be performed
// PARAMETERS: file - Name of the CSV/Binary file passed as argument
// argc - Number of strings pointed to by argv (argument count)
// argv - Array of arguments (argument vector)
// RETURN VALUE: 0/1
//------------------------------------
int main(int argc, char *argv[])
{
   char *fileName = "sales.csv";
   input(&fileName, argc, argv);
   return 0;
}

