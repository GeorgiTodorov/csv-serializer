
#include "structures.c"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//------------------------------------
// DESCRIPTION: Function deserialize from (deserializer.c)
//
// PARAMETERS: binFilePath - Name of the Binary file
// that will be deserialized
// RETURN VALUE: 0/1
//------------------------------------
void logs(char *fileName, char *message);


//------------------------------------
// DESCRIPTION: Function takes one file of type "binary".
// Reads the existing binary file in binary mode and creates a new CSV file in write mode
// Writes as the first row the Header Row that was removed from the Binary.
// Writes back rowId by counting when program switches to next row.
// Using fseek find file position of the stream(file object) to the given offset(bytes per move starting from "whence")Whence is where the offset will be added.
// Using ftell to find the filesize of the binary file. (Returns current file position)
// Using fseek to reset writing pointer to the beginning of the file
// Using fread to read the data that's currently on the pointer.
// While loop to write data into the CSV file until the pointer reaches the last byte of the binary file
// Making sure there isn't unneeded data used by using strlen() and then copying characters from first memory area to the memory destination area
// Repeating for all data
// Before every loop writing the current row with fprintf by using the existing structure and known memory area that it's equal to And increasing count of rowId
// PARAMETERS: binFilePath - Name of the Binary file
// that will be deserialized
// RETURN VALUE: 0/1
//------------------------------------
int deserialize(char *binFilePath) {
    // Open the binary file for reading
    FILE *binFile = fopen(binFilePath, "rb");
    if (!binFile) {
        printf("Error while opening Bin File\n");
        return 1;
    }
     logs("deserializer.c", "Opening Binary file");
    // Create a CSV file in write mode
    FILE *csvFile = fopen("deser.csv", "w");
    if (!csvFile) {
        printf("Error while creating CSV File\n");
        fclose(binFile);
        return 1;
    }
    logs("deserializer.c", "Deserialization starting");

    //Writing as first row the header row back to the CSV file.
    fprintf(csvFile, "\"Row ID\",\"Order ID\",\"Order Date\",\"Customer ID\",\"City\",\"State\",\"Postal Code\",\"Region\",\"Product ID\",\"Category\",\"Sub-Category\",\"Price\"\n");
    //Description written on top
    short rowId = 1;
    fseek(binFile, 0, SEEK_END);
    int fileSize = ftell(binFile);
    fseek(binFile,0, SEEK_SET);
    char* fileBuffer = malloc(fileSize);
    fread(fileBuffer, fileSize, 1, binFile);
    int fileIndex = 0;
    while (fileIndex<fileSize) {
        Row row={0};
        row.rowId = rowId;
        int orderLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.orderId,&fileBuffer[fileIndex],orderLength);
        fileIndex+=orderLength+1;

        int dateLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.orderDate,&fileBuffer[fileIndex],dateLength);
        fileIndex+=dateLength+1;

        int customerIdLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.customerId,&fileBuffer[fileIndex],customerIdLength);
        fileIndex+=customerIdLength+1;

        int cityLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.city,&fileBuffer[fileIndex],cityLength);
        fileIndex+=cityLength+1;

        int stateLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.state,&fileBuffer[fileIndex],stateLength);
        fileIndex+=stateLength+1;

        int postalCodeLength = sizeof(row.postalCode);
        memcpy(&row.postalCode,&fileBuffer[fileIndex],postalCodeLength);
        fileIndex+=postalCodeLength;

        int regionLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.region,&fileBuffer[fileIndex],regionLength);
        fileIndex+=regionLength+1;

        int productIdLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.productId,&fileBuffer[fileIndex],productIdLength);
        fileIndex+=productIdLength+1;

        int categoryLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.category,&fileBuffer[fileIndex],categoryLength);
        fileIndex+=categoryLength+1;

        int subCategoryLength = strlen(&fileBuffer[fileIndex]);
        memcpy(row.subCategory,&fileBuffer[fileIndex],subCategoryLength);
        fileIndex+=subCategoryLength+1;

        int priceLength = sizeof(row.price);
        memcpy(&row.price,&fileBuffer[fileIndex],priceLength);
        fileIndex+=priceLength;

        fprintf(csvFile, "%d,\"%s\",%s,\"%s\",\"%s\",\"%s\",%d,\"%s\",\"%s\",\"%s\",\"%s\",%.2f\n",
                row.rowId, row.orderId, row.orderDate, row.customerId, row.city, row.state,
                row.postalCode, row.region, row.productId, row.category, row.subCategory, row.price);
        rowId+=1;
    }
    logs("deserializer.c", "Deserialization finished");
    fclose(binFile);
    fclose(csvFile);
    return 0;
}
